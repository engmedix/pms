<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SalesController;
use App\Http\Controllers\BackupController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\PurchaseController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\ManufacturerController;
use App\Http\Controllers\ExpenseCategoryController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\StockController;
use App\Http\Controllers\MedicineCategoryController;
use App\Http\Controllers\MedicineController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware'=>['guest']],function (){
    Route::get('login',[LoginController::class,'index'])->name('login');
    Route::post('login',[LoginController::class,'login']);
    Route::get('register',[RegisterController::class,'index'])->name('register');
    Route::post('register',[RegisterController::class,'store']); 

   

    Route::get('forgot-password',[ForgotPasswordController::class,'index'])->name('forgot-password');
    Route::post('forgot-password',[ForgotPasswordController::class,'reset']);
});

Route::group(['middleware'=>['auth']],function (){
    Route::get('dashboard',[DashboardController::class,'index'])->name('dashboard');
    Route::get('logout',[LogoutController::class,'index'])->name('logout');

    Route::get('suppliers',[SupplierController::class,'index'])->name('suppliers');
    Route::get('add-supplier',[SupplierController::class,'create'])->name('add-supplier');
    Route::post('add-supplier',[SupplierController::class,'store']);
    Route::get('suppliers/{supplier}',[SupplierController::class,'show'])->name('edit-supplier');
    Route::delete('suppliers',[SupplierController::class,'destroy']);
    Route::put('suppliers/{supplier}}',[SupplierController::class,'update'])->name('edit-supplier');

    Route::get('purchases',[PurchaseController::class,'index'])->name('purchases');
    Route::get('add-purchase',[PurchaseController::class,'create'])->name('add-purchase');
    Route::post('add-purchase',[PurchaseController::class,'store']);
    Route::get('purchases/{purchase}',[PurchaseController::class,'show'])->name('edit-purchase');
    Route::put('purchases/{purchase}',[PurchaseController::class,'update']);
    Route::delete('purchases',[PurchaseController::class,'destroy']);

    Route::get('sales',[SalesController::class,'index'])->name('sales');
    Route::post('sales',[SalesController::class,'store']);
    Route::put('sales',[SalesController::class,'update']);
    Route::delete('sales',[SalesController::class,'destroy']);

    Route::get('permissions',[PermissionController::class,'index'])->name('permissions');
    Route::post('permissions',[PermissionController::class,'store']);
    Route::put('permissions',[PermissionController::class,'update']);
    Route::delete('permissions',[PermissionController::class,'destroy']);

    Route::get('roles',[RoleController::class,'index'])->name('roles');
    Route::post('roles',[RoleController::class,'store']);
    Route::put('roles',[RoleController::class,'update']);
    Route::delete('roles',[RoleController::class,'destroy']);

    Route::get('users',[UserController::class,'index'])->name('users');
    Route::post('users',[UserController::class,'store']);
    Route::put('users',[UserController::class,'update']);
    Route::delete('users',[UserController::class,'destroy']);

    Route::get('profile',[UserController::class,'profile'])->name('profile');
    Route::post('profile',[UserController::class,'updateProfile']);
    Route::put('profile',[UserController::class,'updatePassword'])->name('update-password');

    Route::get('settings',[SettingController::class,'index'])->name('settings');

    Route::get('notification',[NotificationController::class,'markAsRead'])->name('mark-as-read');
    Route::get('notification-read',[NotificationController::class,'read'])->name('read');

    Route::get('reports',[ReportController::class,'index'])->name('reports');
    Route::post('reports',[ReportController::class,'getData']);

    Route::get('backup',[BackupController::class,'index'])->name('backup-app');
    Route::get('backup-app',[BackupController::class,'database'])->name('backup-db');
});

Route::group(['middleware'=>['auth']],function (){
	//Manufacturers
   // Route::resource('manufacturers', ManufacturerController::class);
   Route::get('manufacturers',[ManufacturerController::class,'index'])->name('manufacturers');
    Route::get('add-manufacturer',[ManufacturerController::class,'create'])->name('add-manufacturer');
    Route::post('add-manufacturer',[ManufacturerController::class,'store']);
    Route::get('manufacturers/{manufacturer}',[ManufacturerController::class,'show'])->name('edit-manufacturer');
    Route::delete('manufacturers',[ManufacturerController::class,'destroy']);
    Route::put('manufacturers/{manufacturer}}',[ManufacturerController::class,'update'])->name('edit-manufacturer');
	
	//Expense Categories
	Route::resource('expense_categories', ExpenseCategoryController::class);
	//Customers
	Route::get('customers',[CustomerController::class,'index'])->name('customers');
    Route::get('add-customer',[CustomerController::class,'create'])->name('add-customer');
    Route::post('add-customer',[CustomerController::class,'store']);
    Route::get('customers/{customer}',[CustomerController::class,'show'])->name('edit-customer');
    Route::delete('customers',[CustomerController::class,'destroy']);
    Route::put('customers/{customer}}',[CustomerController::class,'update'])->name('edit-customer');
	//Invoices
	Route::resource('invoices', InvoiceController::class);
	//Stock
	Route::resource('stocks', StockController::class);

	//Medicine Categories
	Route::get('medicine_categories',[MedicineCategoryController::class,'index'])->name('medicine_categories');
    Route::post('medicine_categories',[MedicineCategoryController::class,'store']);
    Route::put('medicine_categories',[MedicineCategoryController::class,'update']);
    Route::delete('medicine_categories',[MedicineCategoryController::class,'destroy']);
	//Medicine
	//Route::resource('medicine_categories', MedicineCategoryController::class);
	Route::get('medicines',[MedicineController::class,'index'])->name('medicines');
    Route::get('medicines/create',[MedicineController::class,'create'])->name('add-medicine');
    Route::get('expired-medicines',[MedicineController::class,'expired'])->name('expired');
    Route::get('medicines/{medicine}',[MedicineController::class,'show'])->name('edit-medicine');
    Route::get('outstock-medicines',[MedicineController::class,'outstock'])->name('outstock');
    Route::post('medicines/create',[MedicineController::class,'store']);
    Route::post('medicines/{medicine}',[MedicineController::class,'update']);
    Route::delete('medicines',[MedicineController::class,'destroy']);
	Route::get('medicines-add-unit',[MedicineController::class,'add_unit'])->name('add_unit');
	Route::get('medicines-add-type',[MedicineController::class,'add_type'])->name('add_type');
	Route::get('medicines-leaf-settings',[MedicineController::class,'create'])->name('leaf_settings');
	
});

Route::get('/', function () {
    return redirect()->route('dashboard');
});
