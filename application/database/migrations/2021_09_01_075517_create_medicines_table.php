<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicines', function (Blueprint $table) {
            $table->id();
            $table->string('medicine_name');
            $table->string('generic_name',100)->nullable();
            $table->string('box_size',50)->nullable();
            $table->string('category_id',50)->nullable();
            $table->string('type_id',50)->nullable();
            $table->string('manufacturer_id',50)->nullable();
			$table->string('bar_code',100)->nullable();
            $table->string('unit_id',50)->nullable();
			$table->double('price')->nullable();
			$table->double('manufacturer_price')->nullable();
			$table->string('igta',10)->nullable();
			$table->string('vat',10)->nullable();
			$table->string('image',100)->nullable();
			$table->text('details')->nullable();
			$table->string('strength',50)->nullable();
			$table->string('shelf',50)->nullable();
			$table->date('expiry_date',50)->nullable();
			$table->date('manufacture_date',50)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicines');
    }
}
