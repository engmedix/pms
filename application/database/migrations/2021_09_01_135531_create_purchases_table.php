<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->foreignId('medicine_id')->nullable()->constrained()->onDelete("cascade");
            $table->foreignId('manufacturer_id')->nullable()->constrained()->onDelete("cascade");
			$table->string('invoice_number')->nullable();
			$table->string('payment_type')->nullable();
            $table->string('price')->nullable();
            $table->integer('quantity')->nullable();
            $table->date('expiry_date')->nullable();
            $table->string('image')->nullable();
			$table->integer('stock_quantity')->nullable();
			$table->string('box_pattern')->nullable();
			$table->integer('box_quantity')->nullable();
			$table->double('manufacturer_price',15,2)->nullable();
			$table->string('box_mrp')->nullable();
			$table->double('paid_amount',15,2)->nullable();
			$table->double('due_amount',15,2)->nullable();
			$table->string('vat')->nullable();
			$table->string('discount')->nullable();
			$table->double('grand_total',15,2)->nullable();
			$table->string('batch_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
