<?php

namespace App\Http\Controllers;

use App\Models\Manufacturer;
use Illuminate\Http\Request;

class ManufacturerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index()
    {
        $title ="Manufacturer";
        $manufacturers = Manufacturer::get();
        return view('manufacturers.index',compact('title','manufacturers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
		$title = "Add Manufacturer";
        //$products = Product::get();
        return view('manufacturers.create',compact(
            'title'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'email'=>'email|string',
            'phone'=>'max:13',
            'address'=>'required|max:200',
        ]);
        Manufacturer::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'phone'=>$request->phone,
            'city'=>$request->city,
            'address'=>$request->address,
            'country'=>$request->country,
            'previous_balance'=>$request->balance,
        ]);
        $notification = array(
            'message'=>"Manufacturer has been added",
            'alert-type'=>'success',
        );
        return redirect()->route('manufacturers')->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
     public function show(Request $request,$id)
    {
       $title = "Edit Manufacturer";
        $manufacturer = Manufacturer::find($id);
        return view('manufacturers.edit',compact(
            'title','manufacturer'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
  public function edit(Request $request,$id)
    {
         $title = "Edit Manufacturer";
        $manufacturer = Manufacturer::find($id);
        return view('manufacturers.edit',compact(
            'title','manufacturer',
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Manufacturer $manufacturer)
    {
        $this->validate($request,[
            'name'=>'required',
            'email'=>'email|string',
            'phone'=>'max:13',
            'address'=>'required|max:200',
        ]);

        $manufacturer->update($request->all());
        $notification = array(
            'message'=>"Manufacturer has been updated",
            'alert-type'=>'success',
        );
        return redirect()->route('manufacturers')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
   
      public function destroy(Request $request)
    {
        $customer = Manufacturer::find($request->id);
        $customer->delete();
        $notification = array(
            'message'=>"Manufacturer has been deleted",
            'alert-type'=>'success',
        );
        return back()->with($notification);
    }
}
