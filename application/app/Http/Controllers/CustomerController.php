<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title ="Customers";
        $customers = Customer::get();
        return view('customers.index',compact('title','customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
		$title = "Add customer";
        //$products = Product::get();
        return view('customers.create',compact(
            'title'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'email'=>'email|string',
            'phone'=>'max:13',
            'address'=>'required|max:200',
        ]);
        Customer::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'phone'=>$request->phone,
            'city'=>$request->city,
            'address'=>$request->address,
            'country'=>$request->country,
            'previous_balance'=>$request->balance,
        ]);
        $notification = array(
            'message'=>"Customer has been added",
            'alert-type'=>'success',
        );
        return redirect()->route('customers')->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
     public function show(Request $request,$id)
    {
       $title = "Edit Customer";
        $customer = Customer::find($id);
        return view('customers.edit',compact(
            'title','customer'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
  public function edit(Request $request,$id)
    {
         $title = "Edit Customer";
        $customer = Customer::find($id);
        return view('customers.edit',compact(
            'title','customer'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        $this->validate($request,[
            'name'=>'required',
            'email'=>'email|string',
            'phone'=>'max:13',
            'address'=>'required|max:200',
        ]);

        $customer->update($request->all());
        $notification = array(
            'message'=>"Customer has been updated",
            'alert-type'=>'success',
        );
        return redirect()->route('customers')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
   
      public function destroy(Request $request)
    {
        $customer = Customer::find($request->id);
        $customer->delete();
        $notification = array(
            'message'=>"Customer has been deleted",
            'alert-type'=>'success',
        );
        return back()->with($notification);
    }
}
