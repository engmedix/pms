@extends('layouts.app')

@push('page-css')
	<!-- Select2 CSS -->
	<link rel="stylesheet" href="{{asset('assets/plugins/select2/css/select2.min.css')}}">
	<!-- Datetimepicker CSS -->
	<link rel="stylesheet" href="{{asset('assets/css/bootstrap-datetimepicker.min.css')}}">
@endpush

@push('page-header')
<div class="col-sm-12">
	<h3 class="page-title">{{$title}}</h3>
	<ul class="breadcrumb">
		<li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
		<li class="breadcrumb-item active">{{$title}}</li>
	</ul>
</div>
@endpush

@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="card">
			<div class="card-body custom-edit-service">
				
		
			<!-- Add Medicine -->
			<form method="post" enctype="multipart/form-data" action="{{route('edit-manufacturer',$manufacturer)}}">
				@csrf
				@method("PUT")
				<div class="service-fields mb-3">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label>Manufacturer Name<span class="text-danger">*</span></label>
								<input class="form-control" type="text" value="{{$manufacturer->name}}" name="name">
							</div>
						</div>
						<div class="col-lg-6">
							<label>Email Address<span class="text-danger">*</span></label>
							<input class="form-control" type="text" value="{{$manufacturer->email}}" name="email" >
						</div>
					</div>
				</div>

				<div class="service-fields mb-3">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label>Mobile No<span class="text-danger">*</span></label>
								<input class="form-control" type="text" value="{{$manufacturer->phone}}" name="phone">
							</div>
						</div>
						<div class="col-lg-6">
							<label>City<span class="text-danger"></span></label>
							<input class="form-control" type="text" value="{{$manufacturer->city}}" name="city">
						</div>
					</div>
				</div>

				<div class="service-fields mb-3">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label>Country <span class="text-danger">*</span></label>
								<input type="text" name="country" value="{{$manufacturer->country}}" class="form-control">
							</div>
						</div>
						<div class="col-lg-6">
							<label>Address</label>
							<input type="text" name="address" value="{{$manufacturer->address}}" class="form-control">
						</div>
					</div>
				</div>	
				<div class="service-fields mb-3">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label>Previous Balance <span class="text-danger"></span></label>
								<input type="text" name="previous_balance" value="{{$manufacturer->previous_balance}}" class="form-control">
							</div>
						</div>
					</div>
				</div>		
				
				
				<div class="submit-section">
					<button class="btn btn-primary submit-btn" type="submit" name="form_submit" value="submit">Submit</button>
				</div>
			</form>
			<!-- /Add Medicine -->


			</div>
		</div>
	</div>			
</div>
@endsection	

@push('page-js')
	<!-- Datetimepicker JS -->
	<script src="assets/js/moment.min.js"></script>
	<script src="assets/js/bootstrap-datetimepicker.min.js"></script>
	<!-- Select2 JS -->
	<script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>
@endpush

