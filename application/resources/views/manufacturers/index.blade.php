@extends('layouts.app')

@push('page-css')
	<!-- Select2 css-->
	<link rel="stylesheet" href="{{asset('assets/plugins/select2/css/select2.min.css')}}">
@endpush

@push('page-header')
<div class="col-sm-7 col-auto">
	<h3 class="page-title">Manufacturers</h3>
	<ul class="breadcrumb">
		<li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
		<li class="breadcrumb-item active">Manufacturers</li>
	</ul>
</div>
<div class="col-sm-5 col">
	<a href="{{route('add-manufacturer')}}" class="btn btn-primary float-right mt-2">Add New</a>
</div>
@endpush

@section('content')
<div class="row">
	<div class="col-md-12">
	
		<!-- Suppliers -->
		<div class="card">
			<div class="card-body">
				<div class="table-responsive">
					<table id="datatable-export" class="table table-hover table-center mb-0">
						<thead>
							<tr>
								<th>Customer Name</th>
								<th>Mobile No</th>
								<th>Email Address</th>
								<th>Address</th>
								<th>City</th>
								<th>Country</th>
								<th>Balance</th>
								<th class="action-btn">Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($manufacturers as $manufacturer)
							<tr>
								<td>{{$manufacturer->name}}</td>
								<td>{{$manufacturer->phone}}</td>
								<td>{{$manufacturer->email}}</td>
								<td>{{$manufacturer->address}}</td>
								<td>{{$manufacturer->city}}</td>
								<td>{{$manufacturer->country}}</td>
								<td>{{$manufacturer->previous_balance}}</td>
								<td>
									<div class="actions">
										<a class="btn btn-sm bg-success-light" href="{{route('edit-manufacturer',$manufacturer)}}">
											<i class="fe fe-pencil"></i> Edit
										</a>
										<a data-id="{{$manufacturer->id}}" href="javascript:void(0);" class="btn btn-sm bg-danger-light deletebtn" data-toggle="modal">
											<i class="fe fe-trash"></i> Delete
										</a>
									</div>
								</td>
							</tr>
							@endforeach							
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- /Suppliers-->
		
	</div>
</div>
<!-- Delete Modal -->
<x-modals.delete :route="'manufacturers'" :title="'Manufacturer'" />
<!-- /Delete Modal -->
@endsection	

@push('page-js')
	<!-- Select2 js-->
	<script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>
@endpush

