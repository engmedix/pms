<!-- Sidebar -->
<div class="sidebar" id="sidebar">
	<div class="sidebar-inner slimscroll">
		<div id="sidebar-menu" class="sidebar-menu">
			
			<ul>
				<li class="menu-title"> 
					<span>Main</span>
				</li>
				<li class="{{ Request::routeIs('dashboard') ? 'active' : '' }}"> 
					<a href="{{route('dashboard')}}"><i class="fe fe-home"></i> <span>Dashboard</span></a>
				</li>
				
				@can('view-supplier')
				<li class="submenu">
					<a href="#"><i class="fe fe-user"></i> <span>Customers</span> <span class="menu-arrow"></span></a>
					<ul style="display: none;">
						<li><a class="{{ Request::routeIs('customers') ? 'active' : '' }}" href="{{route('customers')}}">Customer List</a></li>
						@can('create-supplier')<li><a class="{{ Request::routeIs('add-customer') ? 'active' : '' }}" href="{{route('add-customer')}}">Add customer</a></li>@endcan
					</ul>
				</li>
				@endcan
				
				<li class="submenu">
					<a href="#"><i class="fe fe-user"></i> <span> Manufacturers</span> <span class="menu-arrow"></span></a>
					<ul style="display: none;">
						<li><a class="{{ Request::routeIs('manufacturers') ? 'active' : '' }}" href="{{route('manufacturers')}}">Manufacturer</a></li>
						<li><a class="{{ Request::routeIs('add-manufacturer') ? 'active' : '' }}" href="{{route('add-manufacturer')}}">Add Manufacturer</a></li>
					</ul>
				</li>
			
				@can('view-products')
				<li class="submenu">
					<a href="#"><i class="fas fa-capsules"></i> <span> Medicine</span> <span class="menu-arrow"></span></a>
					<ul style="display: none;">
					    @can('view-category')<li><a class="{{ Request::routeIs('medicine_categories') ? 'active' : '' }}" href="{{route('medicine_categories')}}">Add Category</a></li>@endcan
					    @can('view-category')<li><a class="{{ Request::routeIs('add_unit') ? 'active' : '' }}" href="{{route('add_unit')}}">Add Unit</a></li>@endcan
					    @can('view-category')<li><a class="{{ Request::routeIs('add_type') ? 'active' : '' }}" href="{{route('add_type')}}">Add Type</a></li>@endcan
					    @can('view-category')<li><a class="{{ Request::routeIs('leaf_settings') ? 'active' : '' }}" href="{{route('leaf_settings')}}">Leaf Settings</a></li>@endcan
						@can('view-products')<li><a class="{{ Request::routeIs(('medicines')) ? 'active' : '' }}" href="{{route('medicines')}}">Medicine</a></li>@endcan
						@can('create-product')<li><a class="{{ Request::routeIs('add-medicine') ? 'active' : '' }}" href="{{route('add-medicine')}}">Add Medicine</a></li>@endcan
						@can('view-outstock-products')<li><a class="{{ Request::routeIs('outstock') ? 'active' : '' }}" href="{{route('outstock')}}">Out-Stock</a></li>@endcan
						@can('view-expired-products')<li><a class="{{ Request::routeIs('expired') ? 'active' : '' }}" href="{{route('expired')}}">Expired </a></li>@endcan
					</ul>
				</li>
				@endcan
				
				@can('view-purchase')
				<li class="submenu">
					<a href="#"><i class="fas fa-dollar-sign"></i> <span> Purchase</span> <span class="menu-arrow"></span></a>
					<ul style="display: none;">
						<li><a class="{{ Request::routeIs('purchases') ? 'active' : '' }}" href="{{route('purchases')}}">Purchase List</a></li>
						@can('create-purchase')
						<li><a class="{{ Request::routeIs('add-purchase') ? 'active' : '' }}" href="{{route('add-purchase')}}">Add Purchase</a></li>
						@endcan
					</ul>
				</li>
				@endcan
				@can('view-purchase')
				<li class="submenu">
					<a href="#"><i class="fas fa-dollar-sign"></i> <span> Invoice</span> <span class="menu-arrow"></span></a>
					<ul style="display: none;">
						@can('create-purchase')
						<li><a class="{{ Request::routeIs('add-purchase') ? 'active' : '' }}" href="{{route('add-purchase')}}">Add Invoice</a></li>
						@endcan
						<li><a class="{{ Request::routeIs('purchases') ? 'active' : '' }}" href="{{route('purchases')}}">POS Invoice</a></li>
						<li><a class="{{ Request::routeIs('purchases') ? 'active' : '' }}" href="{{route('purchases')}}">Invoice List</a></li>
					</ul>
				</li>
				@endcan
				@can('view-products')
				<li class="submenu">
					<a href="#"><i class="fas fa-clinic-medical"></i> <span> Stock</span> <span class="menu-arrow"></span></a>
					<ul style="display: none;">
					    @can('view-outstock-products')<li><a class="{{ Request::routeIs('outstock') ? 'active' : '' }}" href="{{route('outstock')}}">Stock Report</a></li>@endcan
						@can('view-expired-products')<li><a class="{{ Request::routeIs('expired') ? 'active' : '' }}" href="{{route('expired')}}">Available Stock </a></li>@endcan
					</ul>
				</li>
				@endcan
				@can('view-products')
				<li class="submenu">
					<a href="#"><i class="fas fa-clinic-medical"></i> <span> Accounts</span> <span class="menu-arrow"></span></a>
					<ul style="display: none;">
					    @can('view-outstock-products')<li><a class="{{ Request::routeIs('outstock') ? 'active' : '' }}" href="{{route('outstock')}}">Chart of Accounts</a></li>@endcan
						@can('view-expired-products')<li><a class="{{ Request::routeIs('expired') ? 'active' : '' }}" href="{{route('expired')}}">Opening Balance </a></li>@endcan
						@can('view-expired-products')<li><a class="{{ Request::routeIs('expired') ? 'active' : '' }}" href="{{route('expired')}}">Manufacturer Payment </a></li>@endcan
					</ul>
				</li>
				@endcan
				@can('view-sales')
				<li><a class="{{ Request::routeIs('sales') ? 'active' : '' }}" href="{{route('sales')}}"><i class="fe fe-activity"></i> <span>Sales</span></a></li>
				@endcan
				@can('view-reports')
				<li class="submenu">
					<a href="#"><i class="fe fe-document"></i> <span> Reports</span> <span class="menu-arrow"></span></a>
					<ul style="display: none;">
						<li><a class="{{ Request::routeIs('reports') ? 'active' : '' }}" href="{{route('reports')}}">Reports</a></li>
					</ul>
				</li>
				@endcan
				
				<li class="submenu">
					<a href="#"><i class="fe fe-document"></i> <span> Human Resource</span> <span class="menu-arrow"></span></a>
					
					<ul style="display: none;">
						<li><a class="{{ Request::routeIs('reports') ? 'active' : '' }}" href="{{route('reports')}}">Employee</a></li>
					</ul>
					<ul style="display: none;">
						<li><a class="{{ Request::routeIs('reports') ? 'active' : '' }}" href="{{route('reports')}}">Attendance</a></li>
						<ul style="display: none;">
						<li><a class="{{ Request::routeIs('reports') ? 'active' : '' }}" href="{{route('reports')}}">Payroll</a></li>
					</ul>
					</ul>
				</li>
				@can('view-purchase')
				<li class="submenu">
					<a href="#"><i class="fas fa-clinic-medical"></i> <span> Departments</span> <span class="menu-arrow"></span></a>
					<ul style="display: none;">
						<li><a class="">Department</a></li>
						@can('create-purchase')
						<li><a class="">Add Department</a></li>
						@endcan
					</ul>
				</li>
				@endcan
				
				<li class="submenu">
					<a href="#"><i class="fas fa-dollar-sign"></i> <span>Expenses</span> <span class="menu-arrow"></span></a>
					<ul style="display: none;">
						<li><a class="">Expense Category</a></li>
						<li><a class="">Add Expense</a></li>
						<li><a class="">Expense List</a></li>
					</ul>
				</li>
				<!--<li class="submenu">
					<a href="#"><i class="fas fa-prescription"></i> <span>Prescription</span> <span class="menu-arrow"></span></a>
					<ul style="display: none;">
						<li><a class="">Add Prescription</a></li>
						<li><a class="">Prescription List</a></li>
					</ul>
				</li>
				<li class="submenu">
					<a href="#"><i class="fas fa-flask"></i> <span>Lab Tests</span> <span class="menu-arrow"></span></a>
					<ul style="display: none;">
						<li><a class="">Add Lab Tests</a></li>
						<li><a class="">Lab Tests List</a></li>
					</ul>
				</li>
				<li class="submenu">
					<a href="#"><i class="fas fa-bed"></i>  <span>Bed Manager</span> <span class="menu-arrow"></span></a>
					<ul style="display: none;">
						<li><a class="">Bed Category</a></li>
						<li><a class="">Bed List</a></li>
						<li><a class="">Floor</a></li>
					</ul>
				</li>-->
				@can('view-access-control')
				<li class="submenu">
					<a href="#"><i class="fe fe-lock"></i> <span> Access Control</span> <span class="menu-arrow"></span></a>
					<ul style="display: none;">
					    @can('view-users')
				        <li ><a class="{{ Request::routeIs('users') ? 'active' : '' }}" href="{{route('users')}}">Users</a></li>
				        @endcan
						@can('view-permission')
						<li><a class="{{ Request::routeIs('permissions') ? 'active' : '' }}" href="{{route('permissions')}}">Permissions</a></li>
						@endcan
						@can('view-role')
						<li><a class="{{ Request::routeIs('roles') ? 'active' : '' }}" href="{{route('roles')}}">Roles</a></li>
						@endcan
					</ul>
				</li>					
				@endcan	
				<li class="{{ Request::routeIs('profile') ? 'active' : '' }}"> 
					<a href="{{route('profile')}}"><i class="fe fe-user-plus"></i> <span>Profile</span></a>
				</li>
				@can('view-settings')
				<li class="{{ Request::routeIs('settings') ? 'active' : '' }}"> 
					<a href="{{route('settings')}}">
						<i class="fa fa-gears"></i>
						 <span> Settings</span>
					</a>
				</li>
				@endcan
			</ul>
		</div>
	</div>
</div>
<!-- /Sidebar -->